package com.example.auth.demo.kafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

@Slf4j
public class DefaultProducerySync {
    public static void main(String[] args) {
        //0 创建properties
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                "dev01:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.ACKS_CONFIG,"all");

        //1 生产消息
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);
        for (int i = 0; i < 5; i++) {
            kafkaProducer.send(new ProducerRecord<String, String>("first", "Aaa" + i), new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    if (e ==null){
                        System.out.println("============"+recordMetadata.topic()+recordMetadata.partition());
                        log.info("topic is :{} and partitions is : {} ",
                                recordMetadata.topic(),
                                recordMetadata.partition());
                    }
                }
            });
        }
        //关闭流
        kafkaProducer.close();
    }
}
